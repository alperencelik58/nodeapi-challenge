#!/bin/bash

check_secret=$(kubectl get secrets -n nodeapi | grep db-endpoints)
check_deployment=$(kubectl get pods -n nodeapi | awk '{print $3}' | tail -n +2)

slack_webhook=#insert your slack webhook here


if [ -z "$check_secret" ]
then
curl -X POST -H 'Content-type: application/json' --data '{"text":"Secret missing"}' $slack_webhook
fi

if [[ $check_deployment != "Running" ]]
then
curl -X POST -H 'Content-type: application/json' --data '{"text":"Pod is not running. The status of pod is  $check_deployment"}' $slack_webhook
fi


